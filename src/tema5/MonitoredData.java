package tema5;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
public class MonitoredData {
	String activity;
	String start_time;
	String end_time;
	
	public MonitoredData(String s1,String s2, String s3){
		activity = s3;
		start_time = s1;
		end_time = s2;
	}
	@SuppressWarnings("unchecked")
	public static void main(String args[]) throws IOException {
		List<MonitoredData> md;
		Map<String,Integer> map;
		TreeSet<String> treeSet = new TreeSet<>();
		Map<String, Map<String, Integer>> map1;
		Map<String, Long> map2;
		Map<String, List<Long>> map3;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<Long> duration;
		String fileName = "activities.txt";
		md = new ArrayList<>();
	    try (Stream<String> stream = Files.lines(Paths.get(fileName) )){
	    stream.map(line -> line.split("\\s{2,}"))
	          .map(a -> new MonitoredData(a[0], a[1], a[2]))
	          .map(a -> md.add(a))
	          .collect(Collectors.toList());
	    }
	    catch (IOException e){
	       e.printStackTrace();
	    }

	    System.out.println("Task 1 ");
	    for(MonitoredData i : md)
	    	System.out.println(i.start_time +" "+i.end_time+" "+i.activity);
	    System.out.println();
	    System.out.println();
	    
	    
	    System.out.println("Task 2 ");
	    try (Stream<String> stream = Files.lines(Paths.get(fileName) )){
	    	md.stream().map(a -> (a.start_time.split(" "))[0])
	    			   .map(a -> treeSet.add(a))
	    			   .collect(Collectors.toList());
	    	System.out.println(treeSet.size());
	    }
		    catch (IOException e){
		       e.printStackTrace();
		    }
	    
	    
	System.out.println();
    System.out.println();
    
    map = md.stream().collect(Collectors.groupingBy(e -> e.activity, summingInt(e -> 1)));
    System.out.println("Task 3");
    map.forEach((k,v) -> System.out.println(v + "  " + k));
    System.out.println();
    System.out.println();
    
    map1 = md.stream().collect(Collectors.groupingBy( a-> (a.end_time.split(" "))[0],
            Collectors.groupingBy( a-> a.activity, summingInt( a->1))));

   System.out.println("Task 4");
   map.forEach((k,v) -> System.out.println(k + "\n" + v));
   System.out.println();
   System.out.println();
   
   duration = md.stream().map( a->  {
       try{
          return dateFormat.parse(a.end_time).getTime() - dateFormat.parse(a.start_time).getTime();
       }
       catch(ParseException e) { e.printStackTrace();
       return Long.parseLong("0");} }
       ).collect(Collectors.toList());

   SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
   System.out.println("Task 5");
   duration.forEach( a -> System.out.println(simpleDateFormat.format(new Date(a - 2*60*60*1000))));
   System.out.println();
   System.out.println();
   
   map2 = md.stream().collect( Collectors.groupingBy( a -> a.activity, Collectors.summingLong( a->
   { try{ return  dateFormat.parse(a.end_time).getTime() - dateFormat.parse(a.start_time).getTime(); } 
   
   catch(ParseException e) { e.printStackTrace(); return Long.parseLong("0");} }
   )));

   System.out.println("Task 6");
   map2.forEach((k,v)-> System.out.println(v/1000/3600 + ":" + (v/1000 - v/1000/3600*3600)/60 +":" +  (v/1000 - (v/1000 - v/1000/3600*3600)/60*60 - v/1000/3600*3600 +" "+ k )));
   System.out.println();
   System.out.println();
   
   map3 = md.stream().collect( Collectors.groupingBy( a -> a.activity , mapping( a ->{
	   try{ 
		   return  dateFormat.parse(a.end_time).getTime() - dateFormat.parse(a.start_time).getTime();
		   }
   catch(ParseException e){ 
	   e.printStackTrace(); 
	   return Long.parseLong("0");
   } }, toList())));
   System.out.println("Task 7");
   List<String> strings =
   map3.entrySet().stream().map( a ->
          {
              int c = 0;
              for(Long l : a.getValue())
                  if ( l < 5*60*1000 )
                      c++;
               if ( c > 0.9 * a.getValue().size())
                   return a.getKey();
               else
                   return null;
          }).collect( Collectors.toList());

   strings.forEach( a -> { if ( a != null ) System.out.println(a);});
   
}}
